#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#define STUDENTS_COUNT 100

class Student {
	private:
		string studentNo;
		string imie;
		string nazwisko;
		int aktywny;
		
	public:		
		Student(string studentNo) {
			this->studentNo = studentNo;
		}
		
		void setName(string imie) {
			this->imie = imie;
		}
		
		void setSurname(string nazwisko) {
			this->nazwisko = nazwisko;
		}
		void setActive(int aktywny){
			this->aktywny = aktywny;
		}
		
		string getStudentNo() {
			return this->studentNo;
		}
		string getimie() {
			return this->imie;
		}
		string getnazwisko() {
			return this->nazwisko;
		}
		int getactive() {
			return this->aktywny;
		}
};

string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;
	
	ss << randomNumber;
	
	return ss.str();
}

string getRandomimie()
{
	int losowe = rand() % 5;
	string imie;
	switch(losowe)
	{
		case 0:
		imie = "Przemyslaw";
			break;
		case 1:
		imie = "Adam";
			break;
		case 2:
		imie = "Bartosz";
			break;
		case 3:
		imie = "Mateusz";
			break;
		case 4:
		imie = "Sebastian";
			break;
	default:
	imie = "blad";
    break;
	}
	return imie;
}

string getRandomnazwisko()
{
	int losowe = rand() % 5;
	string imie;
	switch(losowe)
	{
		case 0:
		imie = "Nowak";
			break;
		case 1:
		imie = "Kowalski";
			break;
		case 2:
		imie = "Tyka";
			break;
		case 3:
		imie = "Sienkiewicz";
			break;
		case 4:
		imie = "Baran";
			break;
	default:
	imie = "blad";
    break;
	}
	return imie;
}
int randomAktywny()
{
	int losuj = rand() % 2;
	return losuj;
}
int main() {
	vector<Student> students;
	
	for(int i = 0; i < STUDENTS_COUNT; i++) {
		Student student(getRandomStudentNumber());
		student.setName(getRandomimie());
		student.setSurname(getRandomnazwisko());
		student.setActive(randomAktywny());
		students.push_back(student);
	}
	
	cout  << "Students group have been filled." << endl << endl;
	
	for(int i = 0; i < students.size(); i++) {
		if(students.at(i).getactive())
		{
		cout << students.at(i).getnazwisko();
		cout << " ";
		cout << students.at(i).getimie();
		cout << " ";
		cout << students.at(i).getStudentNo() << endl;
		}
	}
	
	return 0;
}
